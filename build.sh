#!/usr/bin/env bash
set -euxo pipefail
cd "$(dirname "$0")"

dockerTag=local/appimage-openvpn
imageCount=$(docker images --filter=reference="$dockerTag" | wc -l)
if [ "$imageCount" = "1" ]; then  # we get 1 line for the table header
  echo "[INFO] docker image missing, building..."
  docker build -t $dockerTag .
fi

outputDir=output
mkdir -p ./$outputDir

if [ ! -d ./openvpn/ ] || [ ! -f ./openvpn/README ]; then
  echo "[INFO] init-ing git submodule for openvpn"
  git submodule update --init
fi

docker run \
  --rm \
  -it \
  -e THE_PREFIX="/build-src" \
  -e OUTPUT_DIR="/build-src/output" \
  -u "$(id -u):$(id -g)" \
  -v "$PWD:/build-src" \
  $dockerTag \
  bash -c 'bash /build-src/inner.sh'

set +x
echo ""
echo "[INFO] all finished, here's the outputs"
ls -l $outputDir/*.AppImage
ls -l $outputDir/*.tar
