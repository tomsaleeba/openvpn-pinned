This dir was created with

```bash
wget https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage
chmod +x linuxdeploy-x86_64.AppImage
# from https://github.com/linuxdeploy/linuxdeploy/issues/86#issuecomment-518026425
sed -i 's|AI\x02|\x00\x00\x00|' linuxdeploy-x86_64.AppImage
./linuxdeploy-x86_64.AppImage --appimage-extract
```

...so we can run the tool inside a docker container.
