A build pipeline to make a version of openvpn that openssl that are known to work with my
infra. The outputs are
- an AppImage
- a tarball with a bash script entrypoint

## How to consume
If you don't want to do all the compilation yourself, see [the Release
page](https://gitlab.com/tomsaleeba/openvpn-pinned/-/releases) of this repo for the
binaries to download.

### Tarball

- download the `.tar` from the releases page
- extract
```bash
tar axf openvpn-2.5.8-w-openssl1.tar
```
- it's ready to run
```bash
./openvpn-2.5.8-w-openssl1/openvpn --version
```

### AppImage

- download the `openvpn-x86_64.AppImage`
- give it exec perms
```bash
chmod +x openvpn-x86_64.AppImage
```
- it's ready to run
```bash
./openvpn-x86_64.AppImage --version
```
- note: the first time you run an AppImage, you might get a UI popup about installing all
  AppImages in a common spot. Make a choice and you'll never see that again and the CLI
  will "just work"


## How to build
To do the compile yourself:

1. clone this repo
1. make sure you have `docker` installed and running (tested against `v20.10`)
1. run `./build.sh`
1. the built binaries are in `output/`

## TODO
Try to compile `openvpn` with statically linked openssl libs, so we don't need to both
with the `tar` version, just use the built binary. [These
instructions](https://gist.github.com/Anubisss/afea82b97058e418e8030ee35e40f54f) look very
helpful, but I haven't been able to get them to work yet. I can build the `.a` files from
openssl (be sure to `make install` to get the `lib/` and `include/` dir structure). But
then when trying to configure openvpn, it can't find openssl
