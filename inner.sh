#!/usr/bin/env bash
# script that runs inside docker to do the heavy-lifting
set -euxo pipefail

thePrefix=${THE_PREFIX:-.}
outputDir=${OUTPUT_DIR:-.}
workDir=$outputDir/work
mkdir -p $workDir

echo -e "\n\n\n"
cd $workDir
dest=$PWD/openssl-bin
if [ ! -f openssl-bin/lib/libssl.so.1.1 ]; then
  echo "[INFO] building openssl"
  tar zxf $thePrefix/openssl/openssl-1.1.1q.tar.gz
  cd openssl-1.1.1q
  ./config --prefix=$dest --openssldir=$dest no-ssl2
  make -j4
  make test
  make install_sw # don't need all the docs and CLI stuff from "make install"
else
  echo "[INFO] openssl already built, *not* rebuilding"
fi

echo -e "\n\n\n"
cd $workDir
if [ ! -f openvpn/src/openvpn/openvpn ]; then
  echo "[INFO] building openvpn"
  cp -r $thePrefix/openvpn .
  cd openvpn/
  autoreconf -i -v -f
  CFLAGS="-I$dest/include -Wl,-rpath=$dest/lib -L$dest/lib" ./configure --disable-lzo
  make -j4
  ldd ./src/openvpn/openvpn  # just for debugging
else
  echo "[INFO] openvpn already built, *not* rebuilding"
fi

echo -e "\n\n\n"
cd $workDir
finalTar=$outputDir/openvpn-2.5.8-w-openssl1.tar
rm -f $finalTar
echo "[INFO] building a simple tar"
mkdir -p openvpn-2.5.8-w-openssl1/{lib,bin}
cd openvpn-2.5.8-w-openssl1
cp ../openssl-bin/lib/{libssl.so.1.1,libcrypto.so.1.1} lib/
cp ../openvpn/src/openvpn/openvpn bin/
cat <<"HEREDOC" > openvpn
#!/usr/bin/env bash
# openvpn v2.5.8 with openssl 1.1.1q
thisDir="$(dirname "$0")"
export LD_LIBRARY_PATH=$thisDir/lib/
$thisDir/bin/openvpn $*
HEREDOC
chmod +x openvpn
cd ..
tar cf $finalTar openvpn-2.5.8-w-openssl1/

echo -e "\n\n\n"
echo "[INFO] building AppImage"
cd "$outputDir"
appDir=$workDir/AppDir
rm -rf $appDir  # rebuilding over existing files isn't consistent
$thePrefix/exploded-linuxdeploy-appimage/squashfs-root/usr/bin/linuxdeploy \
  --executable=$workDir/openvpn/src/openvpn/openvpn \
  --appdir=$appDir \
  --desktop-file=$thePrefix/openvpn.desktop \
  --icon-file=$thePrefix/icon.png \
  --output appimage

echo -e "\n\n\n"
echo "[INFO] inner script finished"
