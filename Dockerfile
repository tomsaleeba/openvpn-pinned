FROM debian:bullseye

RUN \
  apt update && \
  apt install -y --no-install-recommends \
    build-essential `# for general compiling stuff` \
    dh-autoreconf `# for autoreconf command` \
    libpam0g-dev `# needed for openvpn`
